def solveNQueens(n):
    result = []
    # Making use of a helper function to get the
    # solutions in the correct output format
    def create_board(valid_board):
        board = []
        for row in valid_board:
            board.append("".join(row))
        return board

    def backtrack(row, diagonal, anti_diagonal, columns, valid_board):
        # Base case - N queens have been placed
        if row == n:
            result.append(create_board(valid_board))
            return

        for col in range(n):
            cur_dia = row - col
            cur_anti_dia = row + col
            # If the queen is not placeable
            if (col in columns or cur_dia in diagonal or cur_anti_dia in anti_diagonal):
                continue
            # "Add" the queen to the board
            columns.add(col)
            diagonal.add(cur_dia)
            anti_diagonal.add(cur_anti_dia)
            valid_board[row][col] = "1"
            # Move on to the next row with the updated board state(valid_board)
            backtrack(row + 1, diagonal, anti_diagonal, columns, valid_board)
            # "Remove" the queen from the board since we have already
            # explored all valid paths using the above function call
            columns.remove(col)
            diagonal.remove(cur_dia)
            anti_diagonal.remove(cur_anti_dia)
            valid_board[row][col] = "0"

    emptyboard = [["0"] * n for _ in range(n)]
    backtrack(0, set(), set(), set(), emptyboard)
    return result

print(solveNQueens(6))
