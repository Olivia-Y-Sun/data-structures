# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode:
    def __init__(self, value):
        self.value = value
        self.link = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self._length = 0

    @property
    def length(self):
        if self.head == None:
            return 0
        return self._length

    def get(self, index):
        if self.head == None:
            raise IndexError("Index Error") 
        if index < 0:
            index = self._length + index

        node = self.traverse(index)
        return node.value

    def insert(self, value, index=None):
        new_node = LinkedListNode(value)

        if self.head is None:
            self.head = new_node
            self.tail = self.head
        
        elif index is None:
            self.tail.link = new_node
            self.tail = new_node

        elif index == 0:
            # If we are inserting at the beginning of the list:​
            new_node.link = self.head
            self.head = new_node
         
        # If we are inserting anywhere other than the beginning:
        elif index > 0:
            current_node = self.head
            current_index = 0
            # First, we access the node immediately before where the​ new node will go:​
            while current_index < (index - 1):
                current_node = current_node.link
                current_index += 1
            # Have the new node link to the next node:​
            new_node.link = current_node.link
            # Modify the link of the previous node to point to​ our new node:​
            current_node.link = new_node

        self._length += 1

    def remove(self, index):
        remove_node = None

        if self.head == None:
            raise Exception("Empty List")
        # If we are deleting the first node:​
        if index == 0:
            remove_node = self.head
            # Simply set the first node to be what is currently the second node:​
            self.head = self.head.link
            
        else:
            current_node = self.head
            current_index = 0
            # First, we find the node immediately before the one we​
            # want to delete and call it current_node:
            while current_index < (index - 1):
                current_node = current_node.link
                current_index += 1

            remove_node = current_node.link

            if current_node.link == self.tail:
                self.tail = current_node
            else:
                current_node.link = current_node.link.link

        self._length -= 1
        return remove_node.value

    def traverse(self, index):
        if index >= self._length:
            raise IndexError("Index out of range")
        current_node = self.head
        current_index = 0
        while current_index < index:
            current_node = current_node.link
            current_index += 1
        return current_node
